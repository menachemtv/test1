<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
	public static function getCategoryIds()
	{
		$allCategoryIds = self::find()->all();
		$allCategoryIdsArray = ArrayHelper::
					map($allCategoryIds, 'id', 'name');
		return $allCategoryIdsArray;						
	}
	
	public static function getCategoryIdsWithAllCategoryIds()
	{
		$allCategoryIds = self::getCategoryIds();
		$allCategoryIds[-1] = 'All Categories';
		$allCategoryIds = array_reverse ( $allCategoryIds, true );
		return $allCategoryIds;	
	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
