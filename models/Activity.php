<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'categoryId', 'statusId'], 'required'],
            [['id', 'categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */


    /**
     * Defenition of relation to user table
     */ 	
	
	public function getCategoryIdItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
	

	public function getStatusIdItem()
    {
        return $this->hasOne(Status1::className(), ['id' => 'statusId']);
    }
	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
}
