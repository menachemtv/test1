<?php

use yii\db\Migration;

class m160528_094106_insert_into_status extends Migration
{
    public function up()
    {
        $this->insert('status', [
            'name' => 'Lead',
        ]);
        $this->insert('status', [
            'name' => 'Qualified lead',
        ]);	
        $this->insert('status', [
            'name' => 'Customer',
        ]);		
    }

	public function down()
    {
		$this->delete('status', ['name' => 'Lead']);
		$this->delete('status', ['name' => 'Qualified lead']);
		$this->delete('status', ['name' => 'Customer']);
    }

    
}
