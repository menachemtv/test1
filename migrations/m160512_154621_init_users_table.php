<?php

use yii\db\Migration;

class m160512_154621_init_users_table extends Migration
{
    public function up()
    {
		$this->createTable (
		'user',
			[
				'id' => 'pk',
				'username' => 'string',
				'password' => 'string',
				'auth_key' => 'string'
			]
		);
    }

    public function down()
		{
			$this->dropTable('user');
		}

}
