<?php

use yii\db\Migration;

class m160521_155834_alter_users_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
		$this->addColumn('user','email','string');
		$this->addColumn('user','phone','string');
		$this->addColumn('user','created_at','integer');
		$this->addColumn('user','updated_at','integer');
		$this->addColumn('user','created_by','integer');
		$this->addColumn('user','updated_by','integer');
		
    }

    public function down()
    {
        $this->dropTable('user');
    }

}
