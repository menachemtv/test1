<?php

use yii\db\Migration;

class m160503_191921_init_demo_table extends Migration
{
    public function up()
    {
		$this->createTable(
			'student',
			[
				'id' => 'pk',
				'name' => 'string',
			],
			'ENGINE=InnoDB'
		);
    }

    public function down()
    {
       $this->dropTable('student');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
