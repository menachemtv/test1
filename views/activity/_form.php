<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Activity;
use app\models\Category;
use app\models\Status1;
/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'categoryId')->
				dropDownList(Category::getCategoryIds()) ?>

	<?= $form->field($model, 'statusId')->
				dropDownList(Status1::getStatuses()) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
