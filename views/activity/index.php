<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
        //    'categoryId',
        //    'statusId',
            [
				'attribute' => 'statusId',//feild 
				'label' => 'Status Id',//what will apear in form
				'format' => 'raw',
				'value' => function($model){
					return $model->statusItem->name;
				},
				'filter'=>Html::dropDownList('ActivitySearch[statusId]', $statusId, $statusIds, ['class'=>'form-control']),
			],			
            [
				'attribute' => 'categoryId',
				'label' => 'category Id',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->userOwner->fullname, 
					['activity/view', 'id' => $model->userOwner->id]);
				},
				'filter'=>Html::dropDownList('ActivitySearch[categoryId]', $categoryId, $categoryIds, ['class'=>'form-control']),
			],
			//'owner',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
